<?php

/**
 * @file
 * Rules specific functions for Track displayed files module.
 */

/**
 * Implements hook_rules_event_info().
 */
function track_da_files_rules_event_info() {
  return [
    'track_da_files' => [
      'group' => t('Track displayed files'),
      'label' => t('A file has been displayed'),
      'module' => 'track_da_files',
      'variables' => [
        'file' => [
          'type' => 'file',
          'label' => t('Displayed file'),
        ],
        'user' => [
          'type' => 'user',
          'label' => t('User displaying file'),
        ],
      ],
    ],
  ];
}
