<?php

/**
 * @file
 * Views implementation for Track displayed files module.
 */

/**
 * Implements hook_views_data().
 */
function track_da_files_views_data() {
  $table = [];

  /* Track displayed files table */

  // Group identifier.
  $table['track_da_files']['table']['group'] = t('Track da Files');

  // Base table.
  $table['track_da_files']['table']['base'] = [
    'field' => 'recid',
    'title' => t('Track displayed files'),
    'help' => t('Show tracked files display records.'),
  ];
  // Declare how TDF relates to TDFP, users, and file_managed tables.
  $table['track_da_files']['table']['join'] = [
    'track_da_files_paths' => [
      'left_field' => 'pid',
      'field' => 'pid',
      'type' => 'INNER',
    ],
    'users' => [
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'INNER',
    ],
    'file_managed' => [
      'left_table' => 'track_da_files_paths',
      'left_field' => 'pid',
      'type' => 'INNER',
      'field' => 'pid',
    ],
  ];

  // Declare TDF table fields.
  // recid.
  $table['track_da_files']['recid'] = [
    'title' => t('Record ID'),
    'help' => t('The unique record ID of a tracked file.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('Record ID'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on Record ID'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('Record ID'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Record ID'),
    ],
  ];
  // pid.
  $table['track_da_files']['pid'] = [
    'title' => t('Path ID'),
    'help' => t('Path ID is a reference to a unique tracked file path.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('Path ID'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on Path ID'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('Path ID'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Path ID'),
    ],
    'relationship' => [
      'label' => t('Path ID'),
      'base' => 'track_da_files_paths',
      'base field' => 'pid',
    ],
  ];
  // uid.
  $table['track_da_files']['uid'] = [
    'title' => t('User ID'),
    'help' => t('The user who displayed the file.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('User ID'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on User ID'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('User ID'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by User ID'),
    ],
    'relationship' => [
      'title' => t('User information'),
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
    ],
  ];
  // ip.
  $table['track_da_files']['ip'] = [
    'title' => t('IP address'),
    'help' => t('IP address'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('IP address'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on IP address'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('IP address'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by IP address'),
    ],
  ];
  // browser.
  $table['track_da_files']['browser'] = [
    'title' => t('Browser used'),
    'help' => t('Browser used'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('Browser used'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on Browser used'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('Browser used'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Browser used'),
    ],
  ];
  // browser version.
  $table['track_da_files']['browser_version'] = [
    'title' => t('Browser version'),
    'help' => t('Browser version'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('Browser version'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on Browser version'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('Browser version'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Browser version'),
    ],
  ];
  // browser table.
  $table['track_da_files']['browser_platform'] = [
    'title' => t('Browser platform'),
    'help' => t('Browser platform'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('Browser platform'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on Browser platform'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('Browser platform'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Browser platform'),
    ],
  ];
  // browser referer.
  $table['track_da_files']['referer'] = [
    'title' => t('Referer'),
    'help' => t('Referer'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('Referer'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on Referer'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('Referer'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Referer'),
    ],
  ];
  // time.
  $table['track_da_files']['time'] = [
    'title' => t('Time'),
    'help' => t('Time'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'help' => t('Time'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
      'allow empty' => TRUE,
      'help' => t('Filter on Time'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_date',
      'help' => t('Time'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Time'),
    ],
  ];
  // type.
  $table['track_da_files']['type'] = [
    'title' => t('Entity type'),
    'help' => t('The entity type the file is attached to'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('The entity type the file is attached to'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on The entity type the file is attached to'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('The entity type the file is attached to'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by The entity type the file is attached to'),
    ],
  ];
  // id.
  $table['track_da_files']['id'] = [
    'title' => t('Entity ID'),
    'help' => t('The entity id the file is attached to.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('Entity ID'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on Entity ID'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('Entity ID'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Entity ID'),
    ],
  ];

  /* Track displayed files paths */

  // Group identifier.
  $table['track_da_files_paths']['table']['group'] = t('Track da Files paths');
  // Declare how TDFP relates to TDF, users, and file_managed tables.
  $table['track_da_files_paths']['table']['join'] = [
    'track_da_files' => [
      'left_field' => 'pid',
      'field' => 'pid',
      'type' => 'INNER',
    ],
    'file_managed' => [
      'left_field' => 'fid',
      'field' => 'fid',
      'type' => 'INNER',
    ],
  ];
  // pid.
  $table['track_da_files_paths']['pid'] = [
    'title' => t('Path id'),
    'help' => t('The path ID of a tracked file.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('Path id'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on Path id'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('Path id'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Path id'),
    ],
  ];
  // path.
  $table['track_da_files_paths']['path'] = [
    'title' => t('Path'),
    'help' => t('The path of a tracked file'),
    'field' => [
      'handler' => 'views_handler_field',
      'help' => t('Path'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
      'help' => t('Filter on Path'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
      'help' => t('Path'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by Path'),
    ],
  ];
  // fid.
  $table['track_da_files_paths']['fid'] = [
    'title' => t('File ID'),
    'help' => t('The file ID associated with a tracked file path'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'help' => t('File ID'),
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
      'help' => t('Filter on File ID'),
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
      'help' => t('File ID'),
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
      'help' => t('Sort by File ID'),
    ],
  ];

  return $table;
}

/**
 * Implements hook_views_data_alter().
 */
function track_da_files_views_data_alter(&$data) {
  // Declare how TDF relates to the file_managed table.
  $data['file_managed']['table']['join']['track_da_files'] = [
    'left_table' => 'track_da_files_paths',
    'left_field' => 'fid',
    'type' => 'INNER',
    'field' => 'fid',
  ];
}
